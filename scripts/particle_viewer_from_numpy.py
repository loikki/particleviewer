#!/usr/bin/env python3
import sys
import numpy as np
from particle_viewer import ParticleViewer


def read_numpy(filenames):
    pos = np.empty((0, 3))
    for f in filenames:
        tmp = np.load(f)
        pos = np.append(pos, tmp, axis=0)
    return pos

filenames = sys.argv[1:]

pos = read_numpy(filenames)
x = ParticleViewer(pos)
