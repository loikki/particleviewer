#!/usr/bin/env python

from distutils.core import setup
from glob import glob

setup(name='particle_viewer',
      version='0.1',
      description='Python Distribution Utilities',
      author='Loic Hausammann',
      author_email='loic.hausammann@epfl.ch',
      packages=["particle_viewer"],
      scripts=glob("scripts/*")
     )

