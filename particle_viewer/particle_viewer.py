#!/usr/bin/env python3

import numpy as np
import OpenGL
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
from .camera import Camera
from h5py import File

class ParticleViewer:

    def __init__(self, pos):
        # Init GLUT
        glutInit(sys.argv)
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
        glutInitWindowSize(1000, 800)
        glutInitWindowPosition(0, 0)
        self.window = glutCreateWindow("ParticleViewer")
        glutDisplayFunc(self.DrawGLScene)
        glutIdleFunc(self.DrawGLScene)
        glutKeyboardFunc(self.keyPressed)
        glutMouseFunc(self.mouseEvent)
        self.InitGL(1000, 800)

        # Create data
        pos = pos - pos.mean(axis=0)
        self.npoints = len(pos)
        self.buff = glGenBuffers(1)
        glEnable(GL_VERTEX_ARRAY)
        glBindBuffer(GL_ARRAY_BUFFER, self.buff)
        glBufferData(GL_ARRAY_BUFFER, pos.nbytes, pos, GL_STATIC_DRAW)
        glVertexPointer(3, GL_DOUBLE, 0, None)

        # Enable blending
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        # Create variables
        self.point_size = 1
        self.alpha = 1.
        self.mouse = [0, 0]

        # Setup the camera
        x = pos[:, 0].min()
        look = pos.mean(axis=0).astype(np.float32)
        self.setupView([x, 0, 0], look)

        del pos

        self.main()

    def main(self):
        glutMainLoop()

    # The main drawing function.
    def DrawGLScene(self):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()                    # Reset The View

        # Deal with the camera
        pos = self.camera.get_position()
        center = self.camera.get_look_at()
        up = self.camera.get_up()
        gluLookAt(pos[0], pos[1], pos[2],
                  center[0], center[1], center[2],
                  up[0], up[1], up[2])

        # Draw the points
        glPointSize(self.point_size)
        glColor4f(85/256, 170/256, 255/256, self.alpha)
        glDrawArrays(GL_POINTS, 0, self.npoints)

        glutSwapBuffers()

    def InitGL(self, Width, Height):
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClearDepth(1.0)
        glShadeModel(GL_SMOOTH)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, float(Width)/float(Height), 1, 10000000)
        glMatrixMode(GL_MODELVIEW)

    def setupView(self, pos, look):
        self.camera = Camera()
        self.camera.set_position(pos)
        self.camera.look_at(look)

    def keyPressed(self, *args):
        key = args[0].decode("utf8")

        if key == "w":
            self.camera.forward()
        if key == "s":
            self.camera.backward()
        if key == "a":
            self.camera.yaw_minus()
        if key == "d":
            self.camera.yaw_plus()
        if key == "e":
            self.camera.right()
        if key == "q":
            self.camera.left()
        if key == "+":
            self.camera.increase_speed()
        if key == "-":
            self.camera.decrease_speed()
        if key == "o":
            self.point_size /= 2
        if key == "p":
            if self.point_size == 0.:
                self.point_size = 1e-4
            self.point_size *= 2
        if key == "n":
            self.alpha /= 1.2
            if self.alpha <= 0.:
                self.alpha = 1e-6
        if key == "m":
            self.alpha *= 1.2
            if self.alpha > 1.:
                self.alpha = 1.

    def mouseEvent(self, button, mode, x, y):
        if button == GLUT_LEFT_BUTTON:
            if mode == GLUT_DOWN:
                self.mouse = [x, y]
            else:
                dx = x - self.mouse[0]
                dy = y - self.mouse[1]
                width = glutGet(GLUT_WINDOW_X)
                height = glutGet(GLUT_WINDOW_Y)
                self.camera.yaw_plus(0.001 * dx / width)
                self.camera.pitch_plus(0.1 * dy / height)
