import numpy as np
from scipy.spatial.transform import Rotation

angle_speed = 0.1

class Camera:
    def __init__(self):
        self.pos = np.zeros(3, dtype=np.float32)
        self.up = np.array([0, 0, 1], dtype=np.float32)
        self.direction = np.array([1, 0, 0], dtype=np.float32)
        self.pos_speed = 1

    def forward(self):
        self.pos += self.pos_speed * self.direction

    def backward(self):
        self.pos -= self.pos_speed * self.direction

    def get_position(self):
        return self.pos

    def get_up(self):
        return self.up

    def get_look_at(self):
        return self.pos + self.direction

    def set_position(self, pos):
        self.pos[:] = pos

    def look_at(self, look):
        direction = look - self.pos
        direction /= np.sum(direction**2)**0.5
        self.direction[:] = direction

    def yaw_minus(self, speed=angle_speed):
        r = Rotation.from_rotvec(speed * self.up)
        self.direction[:] = r.apply(self.direction)

    def yaw_plus(self, speed=angle_speed):
        r = Rotation.from_rotvec(-speed * self.up)
        self.direction[:] = r.apply(self.direction)

    def pitch_plus(self, speed=angle_speed):
        left = np.cross(self.up, self.direction)
        r = Rotation.from_rotvec(-speed * left)
        self.direction[:] = r.apply(self.direction)

    def left(self):
        left = np.cross(self.up, self.direction)
        self.pos += self.pos_speed * left

    def right(self):
        left = np.cross(self.up, self.direction)
        self.pos -= self.pos_speed * left

    def increase_speed(self):
        if self.pos_speed == 0.:
            self.pos_speed = 1e-4
        self.pos_speed *= 2

    def decrease_speed(self):
        self.pos_speed /= 2
